<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['twitter']], function () {

    Route::get('/tweet', 'TweetController@index');

    Route::get('/tweet/{id}/detail', 'TweetController@detail');

    Route::get('/tweet/post', 'TweetController@post');
    Route::post('/tweet/post', 'TweetController@storePost');

});

Route::get('twitter/login', ['as' => 'twitter.login', 'uses' => 'TwitterOauthController@login']);
Route::get('twitter/oauth', ['as' => 'twitter.callback', 'uses' => 'TwitterOauthController@callback']);

Route::get('twitter/error', ['as' => 'twitter.error', function () {
    // Something went wrong, add your own error handling here
}]);

Route::get('twitter/logout', ['as' => 'twitter.logout', function () {
    Session::forget('access_token');
    return Redirect::to('/')->with('flash_notice', 'You\'ve successfully logged out!');
}]);
