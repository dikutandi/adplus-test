<?php

namespace App\Console\Commands;

use App\Atribut;
use App\Retweet;
use App\Tweets;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Twitter;

class TweetCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tweet:check {what}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Retweet and Favorite From Tweet That Post From This App';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $what   = $this->argument('what');
        $tweets = Tweets::get();
        if ($what == 'atribut') {
            $this->countRetweetAndFavorite($tweets);
        } elseif ($what == 'retweet') {
            $this->getRetweet($tweets);
        }
    }

    public function countRetweetAndFavorite($tweets)
    {
        foreach ($tweets as $tweet) {
            $retweetSum  = $tweet->atribut->sum('retweet');
            $favoriteSum = $tweet->atribut->sum('favorite');

            $detailTweet  = Twitter::getTweet($tweet->id_str);
            $storeAtribut = Atribut::create(
                [
                    'tweets_id' => $tweet->id,
                    'retweet'   => $detailTweet->retweet_count - $retweetSum,
                    'favorite'  => $detailTweet->favorite_count - $favoriteSum,
                ]);
        }
    }

    public function getRetweet($tweets)
    {
        return \DB::transaction(function () use ($tweets) {
            foreach ($tweets as $tweet) {

                $retweetRespone = Twitter::getRts($tweet->id_str);
                foreach ($retweetRespone as $retweet) {
                    $storeRetweet = Retweet::firstOrCreate(
                        [
                            'tweets_id'        => $tweet->id,
                            'retweeted_by'     => $retweet->user->screen_name,
                            'content'          => $retweet->text,
                            'datetime_retweet' => Carbon::parse($retweet->created_at),
                        ]);
                }
                echo $tweet->id_str . 'Retweeted by ' . count($retweetRespone) . "Person \n";
            }
        });
    }
}
