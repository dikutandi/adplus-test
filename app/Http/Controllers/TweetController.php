<?php

namespace App\Http\Controllers;

use App\Atribut;
use App\Http\Requests\TweetRequest;
use App\Tweets;
use Illuminate\Http\Request;
use Twitter;

class TweetController extends Controller
{
    protected $query;

    public function __construct()
    {

    }

    public function index(Request $request)
    {

        $this->query = Tweets::with('atribut');

        if ($request->input('q')) {
            $this->query->where('tweets.content', 'like', '%' . $request->input('q') . '%');
        }

        if ($request->input('name')) {
            $this->query->where('tweets.tweeted_by', '=', $request->input('name'));
        }

        $this->orderBy($request);

        $sorts = [
            'created-desc' => 'Newest',
            'created-asc'  => 'Oldest',
        ];

        $filter = [
            'q'    => $request->get('q'),
            'name' => $request->get('name'),
            'sort' => $request->get('sort'),
        ];

        $tweets = $this->query->paginate(20);

        return view('tweet.lists', compact('tweets', 'filter', 'sorts'));
    }

    public function orderBy(Request $request)
    {
        switch ($request->input('sort')) {
            case 'created-desc':
                $this->query->orderBy('tweets.created_at', 'DESC'); // newest
                break;
            case 'created-asc':
                $this->query->orderBy('tweets.created_at', 'ASC'); // oldest
                break;
            default:
                $this->query->orderBy('tweets.created_at', 'DESC'); // newest
                break;
        }
    }

    public function post(Request $request)
    {
        return view('tweet.post');
    }

    public function detail(Request $request, $id)
    {
        $tweet = Tweets::where('id', $id)->firstOrFail();

        return view('tweet.detail', compact('tweet'));
    }

    public function storePost(TweetRequest $request)
    {
        $tweetPost = Twitter::postTweet(
            [
                'status' => $request->get('tweet'),
                'format' => 'json',
            ]
        );
        $tweetReturn = json_decode($tweetPost);

        try
        {
            return \DB::transaction(function () use ($tweetReturn) {
                $newTweet = Tweets::create([
                    'id_str'     => $tweetReturn->id_str,
                    'tweeted_by' => $tweetReturn->user->screen_name,
                    'content'    => $tweetReturn->text,
                ]);

                $atribut = new Atribut([
                    'retweet'  => 0,
                    'favorite' => 0,
                ]);

                $newTweet->atribut()->save($atribut);

                if ($newTweet) {
                    session()->flash('info', 'Your Tweet Posted Succesfull.');

                    return redirect()->back();
                }
            });

        } catch (\Exception $e) {
            Twitter::destroyTweet($tweetReturn->id_str);
            session()->flash('error', 'Oops !! Something Wrong');

            return redirect()->back();
        }

    }
}
