<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class TwitterMiddleware
{
    protected $except = [
        'TwitterOauthController',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::get('access_token') == null) {
            return redirect('/twitter/login');
        }

        return $next($request);
    }
}
