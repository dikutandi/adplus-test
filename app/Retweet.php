<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retweet extends Model
{
    protected $table = 'retweets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'retweeted_by', 'content', 'tweets_id', 'datetime_retweet',
    ];

    protected $dates = ['created_at'];

    public function Tweet()
    {
        return $this->belongsTo(\App\Tweets::class, 'tweets_id');
    }

}
