<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweets extends Model
{
    protected $table = 'tweets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_str', 'content', 'tweeted_by',
    ];

    protected $dates = ['created_at'];

    public function atribut()
    {
        return $this->hasMany(\App\Atribut::class, 'tweets_id');
    }

    public function retweet()
    {
        return $this->hasMany(\App\Retweet::class, 'tweets_id');
    }

}
