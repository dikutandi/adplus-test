<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atribut extends Model
{
    protected $table = 'atribut';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'retweet', 'favorite', 'tweets_id',
    ];

    public function Tweet()
    {
        return $this->belongsTo(\App\Tweets::class, 'tweets_id');
    }

    protected $dates = ['created_at'];

}
