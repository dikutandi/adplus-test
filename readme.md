# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


Twitter Application with [thujohn/twitter Library](https://github.com/thujohn/twitter) 
## Official Documentation
Create With Laravel 5.3
Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Feature
- Post Twitter Status without go to twitter page
- List Tweet
- Task  Scheduller (Cron) that count Retweet and Favorite from the Tweet
Task  Scheduller (Cron) that get Retweet from the Tweet

## License

open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
