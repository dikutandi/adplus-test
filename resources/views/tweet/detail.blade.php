@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Tweets</b></div>

                <div class="panel-body">
                    Tweeted on <b style="color: #5587da">{{ $tweet->created_at->format('d F Y - H:i') }}</b> <br>
                    <blockquote>
                        {{ $tweet->content }}
                    </blockquote>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><b>History</b></div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th width="40%">Date Time</th>
                            <th width="30%">Num of Retweet</th>
                            <th width="30%">Num of Favorite</th>
                        </tr>
                        @foreach($tweet->atribut as $atribut)
                            <tr>
                                <td>{{ $atribut->created_at->format('d F Y - H:i') }}</td>
                                <td>{{ $atribut->retweet }} times</td>
                                <td>{{ $atribut->favorite }} times</td>
                            </tr>
                        @endforeach
                            <tr style="background-color: #FF81A2">
                                <td><b>Total</b></td>
                                <td><b>{{ $tweet->atribut->sum('retweet') }} times</b></td>
                                <td><b>{{ $tweet->atribut->sum('favorite') }} times</b></td>
                            </tr>
                    </table>
                    <span style="color: #DD013B">New record will be store at {{ $atribut->created_at->addHour()->format('d F Y - H:i') }} </span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Retweet</b></div>

                <div class="panel-body">
                    @foreach($tweet->retweet as $retweet)
                       Retweet by <a href="https://twitter.com/{{ $retweet->retweeted_by }}">{{ '@'.$retweet->retweeted_by }}</a>
                       on <span style="color: #ea87da">{{ $retweet->created_at->format('d F Y - H:i') }} </span>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
