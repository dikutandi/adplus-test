@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Tweet Yuk..!</div>
                    <div class="panel-body">
                        @if(Session::has('info'))
                          <div class="alert alert-success alert-dismissible" role="alert" id="success-alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            {{ Session::get('info') }}
                          </div>
                        @endif
                        @if(Session::has('error'))
                          <div class="alert alert-danger alert-dismissible" role="alert" id="danger-alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            {{ Session::get('error') }}
                          </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/tweet/post') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('tweet') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    {{ Form::textarea('tweet',  old('tweet'), ['placeholder' => 'input your tweet here', 'class' => 'form-control', 'rows' => '2']) }}
                                    @if ($errors->has('tweet'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('tweet') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success pull-right">
                                        Post Tweet
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
