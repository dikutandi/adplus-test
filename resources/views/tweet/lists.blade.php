@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Tweets</b></div>

                <div class="panel-body">
                    @if(count($tweets) > 0)
                    <table class="table table-bordered">
                        <tr>
                            <th width="74%" style="background-color: #eee">Tweet</th>
                            <th width="10%"  style="background-color: #eee">Retweeted</th>
                            <th width="10%"  style="background-color: #eee">Favorited</th>
                            <th width="6%"  style="background-color: #eee">Action</th>
                        </tr>
                        @foreach($tweets as $tweet)
                            <tr>
                                <td>
                                    {{ $tweet->content}}
                                </td>
                                <td>{{ $tweet->atribut->sum('retweet') }}</td>
                                <td>{{ $tweet->atribut->sum('favorite') }}</td>
                                <td>
                                    <a href="{{ url('tweet/'. $tweet->id .'/detail') }}">
                                        See Detail
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="text-center">
                        {!! $tweets->appends($filter)->render() !!}
                    </div>
                    @else
                        No Data
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Filter</b></div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="GET" >
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Keyword</label>

                            <div class="col-md-8">
                                {{ Form::text('q', $filter['q'], ['class'=> 'form-control', 'placeholder' => 'keyword']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Sort By</label>

                            <div class="col-md-8">
                                {{ Form::select('sort', $sorts, $filter['sort'] , ['class'=> 'form-control']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Apply
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
