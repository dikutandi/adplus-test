<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Tweet Application - ADPLUS TEST</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">


    <!-- Styles -->
    <link href="{{ url('/') }}/css/app.css" rel="stylesheet">
    <!-- Generic page styles -->
    <link rel="stylesheet" href="{{ url('/js/vendor/upload/') }}/css/jquery.fileupload.css">

    <!-- Scripts -->
    <script>
        window.Laravel =                                                                                                                         @php
                echo json_encode(['csrfToken' => csrf_token(),]);
            @endphp
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}" style="padding:8px">
                        <img src="http://www.adplus.co.id/assets/img/adplus-top-logo.png" alt="ADPLUS The Most Leading Digital Media Network in Indonesia" style="width:160px">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                            <li><a href="{{ url('/tweet?name='. Session::get('username') ) }}">{{ '@'.Session::get('username') }} Tweet</a></li>
                            <li><a href="{{ url('/tweet') }}">All Tweet</a></li>
                            <li><a href="{{ url('/tweet/post') }}">New Tweet</a></li>
                            <li><a href="{{ url('/twitter/logout') }}" style="color: red">Logout This Twitter Account</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/js/app.js"></script>
</body>
</html>
