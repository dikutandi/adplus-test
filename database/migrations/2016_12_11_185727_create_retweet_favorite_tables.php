<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetweetFavoriteTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retweets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tweets_id')->unsigned();
            $table->string('retweeted_by', 20);
            $table->string('content', 140);
            $table->dateTime('datetime_retweet');

            $table->timestamps();
        });

        // Schema::create('favorites', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('tweets_id')->unsigned();
        //     $table->string('favorited_by', 20);
        //     $table->dateTime('datetime_favorite');

        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retweets');
        // Schema::dropIfExists('favorites');
    }
}
